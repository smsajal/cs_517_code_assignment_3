#include "distribution_mix.h"
#include "random_helper.h"
#include "factory.h"


#include "math.h"

#include <iostream>

#include <cmath>

REGISTER_CLASS( Distribution, DistributionMix )

//TODO fill in

DistributionMix::DistributionMix ( Json::Value &config ) :
        Distribution ( config ),
        _config ( config ) {

    init ( );

}


DistributionMix::~DistributionMix ( ) {

}


void DistributionMix::init ( ) {

    Json::Value &distributions = _config[ "dists" ];
    for ( unsigned int i = 0; i < distributions.size ( ); i++ ) {
        _distributions.push_back ( Factory<Distribution>::create ( distributions[ i ] ));
    }


    Json::Value &probabilities = _config[ "probabilities" ];
    for ( unsigned int i = 0; i < probabilities.size ( ); i++ ) {

        _probabilities.push_back ( probabilities[ i ].asDouble ( ));

    }


    _cumulativeProbabilities = getCumulativeProbabilities ( );

}


std::vector<double> DistributionMix::getCumulativeProbabilities ( ) {

    std::vector<double> cumulativeProbabilities;

    double cumulativeProbabilityValue = 0;


    for ( unsigned int i = 0; i < _probabilities.size ( ); ++i ) {

        cumulativeProbabilityValue += _probabilities[ i ];

        cumulativeProbabilities.push_back ( cumulativeProbabilityValue );
    }

    return cumulativeProbabilities;
}


int DistributionMix::getIndexOfProbability ( double selectionProbability ) {

    int index = 0;
    for ( unsigned int i = 1; i < _probabilities.size ( ); i++ ) {

        if ( selectionProbability > _cumulativeProbabilities[ i - 1 ] && selectionProbability <= _cumulativeProbabilities[ i ] ) {
            index = static_cast<int>(i);
            break;
        }
    }

    return index;

}


double DistributionMix::nextRand ( ) {


    double selectionProbability = uniform01 ( );
    //std::cout << "\n selectionProbability:  " << selectionProbability << "\n";

    unsigned int indexOfProbabilitySelected = ( unsigned int ) getIndexOfProbability ( selectionProbability );


    return _distributions[ indexOfProbabilitySelected ]->nextRand ( );


}


