#ifndef DISTRIBUTION_SUM_H
#define DISTRIBUTION_SUM_H


#include <list>
#include <string>
#include <iterator>

#include "json/json.h"
#include "distribution.h"


class DistributionSum : public Distribution
{
private:
    //TODO fill in
    Json::Value _config;
    std::vector<std::string> _distributionsNames;
    std::vector<Distribution *> _distributions;


public:
    DistributionSum(Json::Value& config);
    virtual ~DistributionSum();
    // Returns next random number
    virtual double nextRand();


    virtual void init();
};

#endif /* DISTRIBUTION_SUM_H */
