#include "perf_model_service_rate.h"
#include "request.h"
#include "time_helper.h"
#include "factory.h"

#include <iostream>

REGISTER_CLASS( PerfModel, PerfModelServiceRate )

//TODO fill in
PerfModelServiceRate::PerfModelServiceRate ( Json::Value &config ) :
        PerfModel ( config ) {

    serviceRate = config[ "serviceRate" ].asDouble ( );


}

PerfModelServiceRate::~PerfModelServiceRate ( ) {

}

uint64_t PerfModelServiceRate::estimateWork ( Request *req ) {

    uint64_t sizeOfRequest = req->getSize ( );


    uint64_t estimatedWork = convertSecondsToTime ( sizeOfRequest / serviceRate );


    return estimatedWork;

}