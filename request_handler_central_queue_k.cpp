#include "request_handler_central_queue_k.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerCentralQueueK )

//TODO fill in
RequestHandlerCentralQueueK::RequestHandlerCentralQueueK( Json::Value &config ) : RequestHandler( config ),
                                                                                  _perfModel(
                                                                                          Factory < PerfModel >::create(
                                                                                                  config[ "perfModel" ] )) {


    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;

    _k = config[ "k" ].asUInt( );


}

RequestHandlerCentralQueueK::~RequestHandlerCentralQueueK( ) {

}

void RequestHandlerCentralQueueK::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {

//    std::cout << "_k = " << _k << "\n";
    req->setRemainingWork( _perfModel );

    _requestsInCentralQueueK.push_back( req );

    if ( _requestsInCentralQueueK.size( ) == 1 ) {
        _startTimeOfCurrentJob = simulator::getSimTime( );
    }

    _remainingTimeOfJobs += req->getRemainingWork( );

    if ( _requestsInCentralQueueK.size( ) <= _k ) {
        uint64_t completionTime = simulator::getSimTime( ) + req->getRemainingWork( );
        addCompletionEvent( completionTime, req );
//        std::cout << "handle------completionTime: " << completionTime << "\n";

    }

    _mappingOfCallbacksToRequests[ req ] = completionCallback;

}

void RequestHandlerCentralQueueK::notifyEnd( Request *req ) {

    _remainingTimeOfJobs -= req->getRemainingWork( );
//    std::cout << "sim-time " << simulator::getSimTime( ) << "\n";
    _requestsInCentralQueueK.erase( _requestsInCentralQueueK.begin( ));

    _startTimeOfCurrentJob = simulator::getSimTime( );

    std::map < Request *, ListenerEnd < Request * > * >::iterator it;
    it = _mappingOfCallbacksToRequests.find( req );

    if ( it != _mappingOfCallbacksToRequests.end( )) {

        ListenerEnd < Request * > *completionCallBackToBeRemoved = it->second;
        _mappingOfCallbacksToRequests.erase( it );

        notifyListenersEnd( req );

        completionCallBackToBeRemoved->notifyEnd( req );

    } else {
        _remainingTimeOfJobs += req->getRemainingWork( );
    }

    //todo: calculate completion time for the last job
//    std::cout << "queue size: " << _requestsInCentralQueueK.size( ) << "\n";

    if ( _requestsInCentralQueueK.size( ) == _k ) {

        Request *request = _requestsInCentralQueueK[ _requestsInCentralQueueK.size( ) - 1 ];

        uint64_t completionTime = simulator::getSimTime( ) + request->getRemainingWork( );
//        std::cout << "notify$$$$$$$completionTime: " << completionTime << "\n";
        addCompletionEvent( completionTime, request );
    }
//    simulator::EventReference eventReference=addCompletionEvent( completionTime, request );



    return;


}

unsigned int RequestHandlerCentralQueueK::getQueueLength( ) {

    return ( unsigned int ) _requestsInCentralQueueK.size( );

}

uint64_t RequestHandlerCentralQueueK::getRemainingWorkLeft( ) {

    if ( _requestsInCentralQueueK.size( ) == 0 ) {

        return 0;

    }

    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;

    return _remainingTimeOfJobs - doneTimeOfCurrentJob;

}