#ifndef REQUEST_HANDLER_CENTRAL_QUEUE_K_H
#define REQUEST_HANDLER_CENTRAL_QUEUE_K_H

#include <vector>
#include <map>


#include "request_handler.h"
#include "request.h"
#include "listener.h"
#include "json/json.h"
#include "perf_model.h"

class RequestHandlerCentralQueueK : public RequestHandler
{
private:
    //TODO fill in
    std::vector<Request *> _requestsInCentralQueueK;
    std::map<Request*, ListenerEnd<Request *>* > _mappingOfCallbacksToRequests;

    uint64_t _startTimeOfCurrentJob;
    uint64_t _remainingTimeOfJobs;
    uint _k;

    PerfModel *_perfModel;


public:
    RequestHandlerCentralQueueK(Json::Value& config);
    virtual ~RequestHandlerCentralQueueK();

    // Handle request sent to server
    virtual void handleRequest(Request* req, ListenerEnd<Request*>* completionCallback);

    // Called when request is complete
    virtual void notifyEnd(Request* req);

    // Get current queue length
    virtual unsigned int getQueueLength();

    // Get remaining work left in queue in nanoseconds
    virtual uint64_t getRemainingWorkLeft();
};

#endif /* REQUEST_HANDLER_CENTRAL_QUEUE_K_H */
