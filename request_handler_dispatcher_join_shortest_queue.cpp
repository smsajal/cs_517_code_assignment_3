#include "request_handler_dispatcher_join_shortest_queue.h"
#include "factory.h"

#include <limits>

REGISTER_CLASS( RequestHandler, RequestHandlerDispatcherJoinShortestQueue )

//TODO fill in

RequestHandlerDispatcherJoinShortestQueue::RequestHandlerDispatcherJoinShortestQueue ( Json::Value &config )
        : RequestHandlerDispatcher ( config ) {


}

RequestHandlerDispatcherJoinShortestQueue::~RequestHandlerDispatcherJoinShortestQueue ( ) {

}

unsigned int RequestHandlerDispatcherJoinShortestQueue::selectRequestHandler ( const Request *req,
                                                                               const std::vector<RequestHandler *> &reqHandlers ) {

    unsigned int indexToBeReturned = 999999;
    unsigned int minimumQueueLength = std::numeric_limits<int>::max ( );
    for ( unsigned int i = 0; i < reqHandlers.size ( ); i++ ) {

        if ( minimumQueueLength >= reqHandlers[ i ]->getQueueLength ( )) {

            indexToBeReturned = i;
            minimumQueueLength = reqHandlers[ i ]->getQueueLength ( );

        }
    }

    return indexToBeReturned;
}
