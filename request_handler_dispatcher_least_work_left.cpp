#include "request_handler_dispatcher_least_work_left.h"
#include "factory.h"

#include <limits>

REGISTER_CLASS( RequestHandler, RequestHandlerDispatcherLeastWorkLeft )

//TODO fill in

RequestHandlerDispatcherLeastWorkLeft::RequestHandlerDispatcherLeastWorkLeft ( Json::Value &config )
        : RequestHandlerDispatcher ( config ) {


}

RequestHandlerDispatcherLeastWorkLeft::~RequestHandlerDispatcherLeastWorkLeft ( ) {


}

unsigned int RequestHandlerDispatcherLeastWorkLeft::selectRequestHandler ( const Request *req,
                                                                           const std::vector<RequestHandler *> &reqHandlers ) {

    unsigned int indexOfRequestHandlerWithLeastWork = 999999;
    uint64_t leastAmountOfWork = std::numeric_limits<uint64_t>::max ( );

    for ( unsigned int i = 0; i < reqHandlers.size ( ); i++ ) {

        if ( leastAmountOfWork >= reqHandlers[ i ]->getRemainingWorkLeft ( )) {

            leastAmountOfWork = reqHandlers[ i ]->getRemainingWorkLeft ( );
            indexOfRequestHandlerWithLeastWork = i;


        }

    }

    return indexOfRequestHandlerWithLeastWork;

}