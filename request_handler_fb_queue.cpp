#include "request_handler_fb_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerFbQueue )

//TODO fill in

RequestHandlerFbQueue::RequestHandlerFbQueue( Json::Value &config ) : RequestHandler( config ), _perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {


    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;
    _indexInserted = 0;
    _lastIncomingEventTime = 0;
    _completionTimeOfReq = 0;

}

RequestHandlerFbQueue::~RequestHandlerFbQueue( ) {

}


void RequestHandlerFbQueue::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {

//    std::cout << "\n\n\n$$$$$$$$$$$$$$$$$$$$$$$$$$$   ARRIVAL   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
//    std::cout << "simtime: " << simulator::getSimTime( ) << " arrival: " << req->getArrivalTime( ) << "  size: "
//              << req->getSize( ) << "\n";

    req->setRemainingWork( _perfModel );

    if ( _requestsInFBQueue.size( ) == 0 ) {

        _requestsInFBQueue.push_back( req );

        _updateAttainedServiceForRequest( req );

        uint64_t completionTime = simulator::getSimTime( ) + req->getRemainingWork( );

        simulator::EventReference eventReference = addCompletionEvent( completionTime, req );

        _mappingOfEventReferenceToRequests[ req ] = eventReference;

        _indexInserted = 0;

    } else {

        _setIncomingRequestInCorrectPosition( req );

        _modifyCompletionTimeForAllJobs( req );
    }

    _remainingTimeOfJobs += req->getRemainingWork( );


    _mappingOfCallbacksToRequests[ req ] = completionCallback;

    if ( simulator::getSimTime( ) > _lastIncomingEventTime ) {

        _lastIncomingEventTime = simulator::getSimTime( );
    }
//    for ( unsigned long i = 0 ; i < _requestsInFBQueue.size( ) ; i++ ) {
//        auto it = _mappingOfEventReferenceToRequests.find( _requestsInFBQueue[ i ] );
//        std::cout << "arrival: " << _requestsInFBQueue[ i ]->getArrivalTime( ) << " size: "
//                  << _requestsInFBQueue[ i ]->getSize( ) << " attainedService: "
//                  << _requestsInFBQueue[ i ]->getSize( ) - _requestsInFBQueue[ i ]->getRemainingWork( )
//                  << " completionTime: "
//                  << it->second->timestamp << "\n";
//    }

}

void RequestHandlerFbQueue::notifyEnd( Request *req ) {

//    std::cout<<"Im here ..\n";

    _remainingTimeOfJobs -= req->getRemainingWork( );

    auto it = std::find( _requestsInFBQueue.begin( ), _requestsInFBQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != _requestsInFBQueue.end( )) {
        indexOfRequest = static_cast<int>(std::distance( _requestsInFBQueue.begin( ), it ));
    }

    if ( indexOfRequest == -1 ) {
        _remainingTimeOfJobs += req->getRemainingWork( );
        return;
    }

    _requestsInFBQueue.erase( _requestsInFBQueue.begin( ) + indexOfRequest );

    auto itr = _mappingOfCallbacksToRequests.find( req );

    if ( itr != _mappingOfCallbacksToRequests.end( )) {
        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;

        _mappingOfCallbacksToRequests.erase( itr );

        notifyListenersEnd( req );

        completionCallBackToBeRemoved->notifyEnd( req );
    }


    auto itr2 = _mappingOfEventReferenceToRequests.find( req );

    if ( itr2 != _mappingOfEventReferenceToRequests.end( )) {
        _mappingOfEventReferenceToRequests.erase( itr2 );
    }

    return;
}

unsigned int RequestHandlerFbQueue::getQueueLength( ) {
    return ( unsigned int ) _requestsInFBQueue.size( );
}

uint64_t RequestHandlerFbQueue::getRemainingWorkLeft( ) {

    if ( _requestsInFBQueue.size( ) == 0 ) {
        return 0;
    }

    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
    return _remainingTimeOfJobs - doneTimeOfCurrentJob;
}
//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

void RequestHandlerFbQueue::_setIncomingRequestInCorrectPosition( Request *req ) {
//    std::cout << "lastIncomingEventTime: " << _lastIncomingEventTime << "\n";
    if ( _lastIncomingEventTime < simulator::getSimTime( )) {
        //todo: update the attained service of the requests currently in the queue
        //fixme: if all the jobs have the same service attained, then processor sharing
        //fixme: else, incrementally set the completionTime
        if ( _doAllJobsHaveSameAttainedService( )) {
            uint64_t serviceAttainedByEachReq =
                    (simulator::getSimTime( ) - _lastIncomingEventTime) / _requestsInFBQueue.size( );


            for ( unsigned long i = 0 ; i < _requestsInFBQueue.size( ) ; i++ ) {

                Request *requestInConsideration = _requestsInFBQueue[ i ];

                uint64_t oldCompletionTime = _mappingOfEventReferenceToRequests[ requestInConsideration ]->timestamp;

                auto iterator = _mappingOfEventReferenceToRequests.find( requestInConsideration );
                if ( iterator != _mappingOfEventReferenceToRequests.end( )) {

                    simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
                    _mappingOfEventReferenceToRequests.erase( iterator );

                }

                uint64_t remainingWorkOfRequestInConsideration =
                        requestInConsideration->getRemainingWork( ) - serviceAttainedByEachReq;
                requestInConsideration->setRemainingWork( remainingWorkOfRequestInConsideration );

                simulator::EventReference newEventReference = addCompletionEvent( oldCompletionTime,
                                                                                  requestInConsideration );
                _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;
            }
        } else {

            uint64_t completionTime = simulator::getSimTime( );
            for ( unsigned long i = 0 ; i < _requestsInFBQueue.size( ) ; i++ ) {

                Request *requestInConsideration = _requestsInFBQueue[ i ];

                auto iterator = _mappingOfEventReferenceToRequests.find( requestInConsideration );
                if ( iterator != _mappingOfEventReferenceToRequests.end( )) {
                    simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
                    _mappingOfEventReferenceToRequests.erase( iterator );
                }

                completionTime += requestInConsideration->getRemainingWork( );
                if ( i == 0 ) {

                    uint64_t remainingWorkOfConsideredReq = requestInConsideration->getRemainingWork()-(simulator::getSimTime()-requestInConsideration->getArrivalTime());
                    requestInConsideration->setRemainingWork( remainingWorkOfConsideredReq);


                }
                simulator::EventReference newEventReference = addCompletionEvent(completionTime,requestInConsideration);
                _mappingOfEventReferenceToRequests[requestInConsideration]=newEventReference;


            }

        }


    }

    _lastIncomingEventTime = simulator::getSimTime( );

    if ( _requestsInFBQueue[ 0 ]->getSize( ) - _requestsInFBQueue[ 0 ]->getRemainingWork( ) > 0 ) {
        _requestsInFBQueue.insert( _requestsInFBQueue.begin( ), req );
//        _mappingOfAttainedServiceToRequests[ req ] = 0;

        _indexInserted = 0;


//        req->setAttainedService(0);
    } else {

        std::vector < Request * > v2( _requestsInFBQueue );

        v2.insert( v2.begin( ), req );
        std::sort( v2.begin( ), v2.end( ), _compareRequests );

        auto it = std::find( v2.begin( ), v2.end( ), req );
        int idx = -1;
        if ( it != v2.end( )) {
            idx = static_cast<int>(std::distance( v2.begin( ), it ));
        }
        if ( idx != -1 ) {
            _requestsInFBQueue.insert( _requestsInFBQueue.begin( ) + idx, req );
            _indexInserted = static_cast<unsigned long>(idx);
        }
    }

    return;


}

void RequestHandlerFbQueue::_modifyCompletionTimeForAllJobs( Request *req ) {

    if ( _indexInserted != 0 ) {
        //todo: calculate via processor sharing

        std::map < Request *, uint64_t > mappingOfTemporaryRemainingTimeToRequests;

        for ( unsigned long i = 0 ; i < _requestsInFBQueue.size( ) ; i++ ) {
            Request *request = _requestsInFBQueue[ i ];
            mappingOfTemporaryRemainingTimeToRequests[ request ] = request->getRemainingWork( );
        }

        unsigned long jobCountInQueue = _requestsInFBQueue.size( );
        uint64_t newCompletionTime = simulator::getSimTime( );

        for ( unsigned long i = 0 ; i < _requestsInFBQueue.size( ) ; i++, jobCountInQueue-- ) {

            Request *requestInConsideration = _requestsInFBQueue[ i ];

            auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );

            if ( itr != _mappingOfEventReferenceToRequests.end( )) {

                simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
                _mappingOfEventReferenceToRequests.erase( itr );
            }

            newCompletionTime += (mappingOfTemporaryRemainingTimeToRequests[ requestInConsideration ] *
                                  jobCountInQueue);


            simulator::EventReference newEventReference = addCompletionEvent( newCompletionTime,
                                                                              requestInConsideration );
            _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;


            for ( unsigned long j = i + 1 ; j < _requestsInFBQueue.size( ) ; j++ ) {
                mappingOfTemporaryRemainingTimeToRequests[ _requestsInFBQueue[ j ]] =
                        _requestsInFBQueue[ j ]->getRemainingWork( ) - requestInConsideration->getRemainingWork( );
            }


        }


    } else {
        //todo: first calculate diff with the next job, add that to completion time
//        uint64_t differenceInAttainedServiceWithNextJob = _mappingOfAttainedServiceToRequests[ _requestsInFBQueue[ 1 ]];
        uint64_t differenceInAttainedServiceWithNextJob =
                _requestsInFBQueue[ 1 ]->getSize( ) - _requestsInFBQueue[ 1 ]->getRemainingWork( );
//        std::cout << "diff: " << differenceInAttainedServiceWithNextJob << "\n";
        uint64_t newCompletionTime;
        unsigned long iterationStartingJobIndex;
//        std::cout << "size: " << _requestsInFBQueue[ 0 ]->getSize( ) << "\n";
        if ( differenceInAttainedServiceWithNextJob <= _requestsInFBQueue[ 0 ]->getSize( )) {

            newCompletionTime = simulator::getSimTime( ) + differenceInAttainedServiceWithNextJob;

            simulator::EventReference eventReference = addCompletionEvent( newCompletionTime, req );
            _mappingOfEventReferenceToRequests[ req ] = eventReference;

            iterationStartingJobIndex = 1;

        } else {
            newCompletionTime = simulator::getSimTime( ) + differenceInAttainedServiceWithNextJob;
            iterationStartingJobIndex = 0;

        }

        std::map < Request *, uint64_t > mappingOfTemporaryRemainingTimeToRequests;
        for ( unsigned long i = iterationStartingJobIndex ; i < _requestsInFBQueue.size( ) ; i++ ) {

            Request *request = _requestsInFBQueue[ i ];
            mappingOfTemporaryRemainingTimeToRequests[ request ] = request->getRemainingWork( );
        }

        unsigned long jobCount = _requestsInFBQueue.size( ) - iterationStartingJobIndex;

        for ( unsigned long i = iterationStartingJobIndex ; i < _requestsInFBQueue.size( ) ; i++, jobCount-- ) {

            Request *requestInConsideration = _requestsInFBQueue[ i ];
            auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );

            if ( itr != _mappingOfEventReferenceToRequests.end( )) {

                simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
                _mappingOfEventReferenceToRequests.erase( itr );

            }

//            uint64_t workLeftOfRequestInConsideration = requestInConsideration->getRemainingWork( ) -
//                                                        _mappingOfAttainedServiceToRequests[ requestInConsideration ];
            //todo: modify this line---maybe not...
            uint64_t workLeftOfRequestInConsideration = requestInConsideration->getRemainingWork( );


            //newCompletionTime += (workLeftOfRequestInConsideration * jobCount);
            newCompletionTime += workLeftOfRequestInConsideration;

            simulator::EventReference newEventReference = addCompletionEvent( newCompletionTime,
                                                                              requestInConsideration );
            _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;


            uint64_t serviceAttainedOnCompletion =
                    (newCompletionTime - simulator::getSimTime( )) / _requestsInFBQueue.size( );
            for ( unsigned long j = i + 1 ; j < _requestsInFBQueue.size( ) ; j++ ) {

//                _mappingOfAttainedServiceToRequests[ _requestsInFBQueue[ j ]] += (serviceAttainedOnCompletion);
                mappingOfTemporaryRemainingTimeToRequests[ _requestsInFBQueue[ j ]] -= serviceAttainedOnCompletion;

            }


        }
        //todo: then check if job remains, and start adding that
        //todo: then run a loop
    }

    return;
}

bool RequestHandlerFbQueue::_isTwoRequestsEqual( Request *req1, Request *req2 ) {

    bool arrivalTimeEqual = (req1->getArrivalTime( ) == req2->getArrivalTime( ));

    bool sizeEqual = (req1->getSize( ) == req2->getSize( ));

    return (arrivalTimeEqual && sizeEqual);
}

void RequestHandlerFbQueue::_updateAttainedServiceForRequest( Request *request ) {

    if ( _requestsInFBQueue.size( ) == 1 ) {
        _mappingOfAttainedServiceToRequests[ request ] = 0;
        return;
    }
    //todo: calculate the new attained service for the job

}

bool RequestHandlerFbQueue::_doAllJobsHaveSameAttainedService( ) {
    for( unsigned long i=0;i<_requestsInFBQueue.size()-1;i++){

        uint64_t serviceForCurrentJob = _requestsInFBQueue[i]->getSize()-_requestsInFBQueue[i]->getRemainingWork();
        uint64_t serviceForNextJob = _requestsInFBQueue[i+1]->getSize()-_requestsInFBQueue[i+1]->getRemainingWork();

        if(serviceForCurrentJob!=serviceForNextJob){
            return false;
        }
    }
    return true;
}


