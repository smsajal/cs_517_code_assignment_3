#ifndef REQUEST_HANDLER_FB_QUEUE_H
#define REQUEST_HANDLER_FB_QUEUE_H

#include "request_handler.h"
#include "request.h"
#include "listener.h"
#include "json/json.h"
#include "perf_model.h"
#include "simulator.h"

class RequestHandlerFbQueue : public RequestHandler {
private:
    //TODO fill in
    std::vector < Request * > _requestsInFBQueue;
    std::map < Request *, ListenerEnd < Request * > * > _mappingOfCallbacksToRequests;
    std::map < Request *, simulator::EventReference > _mappingOfEventReferenceToRequests;
//    std::map < Request *, uint64_t > _mappingOfCompletionTimeToRequests;
    std::map < Request *, uint64_t > _mappingOfAttainedServiceToRequests;

    uint64_t _startTimeOfCurrentJob;
    uint64_t _remainingTimeOfJobs;
    unsigned long _indexInserted;

    uint64_t _lastIncomingEventTime;
    PerfModel *_perfModel;

    uint64_t _completionTimeOfReq;

    virtual void _setIncomingRequestInCorrectPosition( Request *req );

    virtual void _modifyCompletionTimeForAllJobs( Request *req );

    virtual bool _isTwoRequestsEqual( Request *req1, Request *req2 );

    virtual void _updateAttainedServiceForRequest( Request *request );
    virtual bool _doAllJobsHaveSameAttainedService();

    static bool _compareRequests( Request *req1, Request *req2 ) {
        //todo: modify this, maybe?

        uint64_t attainedServiceOfReq1 = req1->getSize( ) - req1->getRemainingWork( );

        uint64_t attainedServiceOfReq2 = req2->getSize( ) - req2->getRemainingWork( );

        if ( attainedServiceOfReq1 == attainedServiceOfReq2 ) {

            return req1->getSize( ) < req2->getSize( );

        } else {

            return attainedServiceOfReq1 < attainedServiceOfReq2;

        }


    }


public:
    RequestHandlerFbQueue( Json::Value &config );

    virtual ~RequestHandlerFbQueue( );

    // Handle request sent to server
    virtual void handleRequest( Request *req, ListenerEnd < Request * > *completionCallback );

    // Called when request is complete
    virtual void notifyEnd( Request *req );

    // Get current queue length
    virtual unsigned int getQueueLength( );

    // Get remaining work left in queue in nanoseconds
    virtual uint64_t getRemainingWorkLeft( );
};

#endif /* REQUEST_HANDLER_FB_QUEUE_H */
