#include "request_handler_lcfs_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerLcfsQueue )

//TODO fill in
RequestHandlerLcfsQueue::RequestHandlerLcfsQueue( Json::Value &config ) : RequestHandler( config ), _perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {

    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;

}

RequestHandlerLcfsQueue::~RequestHandlerLcfsQueue( ) {

}

void RequestHandlerLcfsQueue::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {

    req->setRemainingWork( _perfModel );

    //todo: have to send it to the first/second place in queue
    if ( _requestsInLCFSQueue.size( ) == 0 ) {
        _requestsInLCFSQueue.push_back( req );
    } else {
        _requestsInLCFSQueue.insert( _requestsInLCFSQueue.begin( ) + 1, req );
    }
    if ( _requestsInLCFSQueue.size( ) == 1 ) {
        _startTimeOfCurrentJob = simulator::getSimTime( );

    }
    //todo: have to update the remainingTimeOfJobs accordingly (its the same as before)
    _remainingTimeOfJobs += req->getRemainingWork( );



    //todo: calculate completion time for the incoming job
    uint64_t completionTime;
    if ( _requestsInLCFSQueue.size( ) == 1 ) {
        completionTime = _startTimeOfCurrentJob + req->getRemainingWork( );
    } else {
        uint64_t remainingTimeOfCurrentJob =
                (_startTimeOfCurrentJob + _requestsInLCFSQueue[ 0 ]->getRemainingWork( ));
        completionTime = remainingTimeOfCurrentJob + req->getRemainingWork( );
    }
    //todo: map eventReference to Req
    simulator::EventReference eventReference = addCompletionEvent( completionTime, req );

    _mappingOfEventReferenceToRequests[ req ] = eventReference;


    _mappingOfCallbacksToRequests[ req ] = completionCallback;

    //todo: modify completionTimes for the previous events accordingly
    _modifyCompletionTimeForFollowingJobs(completionTime);

//    std::cout << "inside handle request----9\n";

    return;

}

void RequestHandlerLcfsQueue::notifyEnd( Request *req ) {

//    std::cout << "inside notify end....1\n";
    _remainingTimeOfJobs -= req->getRemainingWork( );

    //todo: remove that element from the vector
    auto it = std::find( _requestsInLCFSQueue.begin( ), _requestsInLCFSQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != _requestsInLCFSQueue.end( )) {
        indexOfRequest = static_cast<int>(std::distance( _requestsInLCFSQueue.begin( ), it ));
    }
//    std::cout << "inside notify end....2\n";
    if ( indexOfRequest == -1 ) {
//        std::cout << "request not found\n";
        _remainingTimeOfJobs += req->getRemainingWork( );
        return;
    }
//    std::cout << "inside notify end....3\n";
    _requestsInLCFSQueue.erase( _requestsInLCFSQueue.begin( ) + indexOfRequest );
    //todo: vector element is removed


    _startTimeOfCurrentJob = simulator::getSimTime( );
//    std::cout << "inside notify end....4\n";

    auto itr = _mappingOfCallbacksToRequests.find( req );

    if ( itr != _mappingOfCallbacksToRequests.end( )) {

        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;
        _mappingOfCallbacksToRequests.erase( itr );
//        std::cout << "inside notify end....5\n";
        notifyListenersEnd( req );

        completionCallBackToBeRemoved->notifyEnd( req );
    }
//    std::cout << "inside notify end....6\n";


    auto itr2 = _mappingOfEventReferenceToRequests.find( req );

    if ( itr2 != _mappingOfEventReferenceToRequests.end( )) {
//        std::cout << "inside notify end....7\n";
        _mappingOfEventReferenceToRequests.erase( itr2 );

    }

//    std::cout << "inside notify end....8\n";
    return;
}

unsigned int RequestHandlerLcfsQueue::getQueueLength( ) {

    return ( unsigned int ) _requestsInLCFSQueue.size( );
}

uint64_t RequestHandlerLcfsQueue::getRemainingWorkLeft( ) {

    if ( _requestsInLCFSQueue.size( ) == 0 ) {
        return 0;
    }
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;

    return _remainingTimeOfJobs - doneTimeOfCurrentJob;

}

void RequestHandlerLcfsQueue::_modifyCompletionTimeForFollowingJobs( uint64_t completionTime ) {

    for ( unsigned long i = 2 ; i < _requestsInLCFSQueue.size( ) ; i++ ) {
        Request *requestInConsideration = _requestsInLCFSQueue[ i ];

        auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );

        if ( itr != _mappingOfEventReferenceToRequests.end( )) {


            simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
            _mappingOfEventReferenceToRequests.erase( itr );

        }

        completionTime += requestInConsideration->getRemainingWork( );




        simulator::EventReference newEventReference = addCompletionEvent( completionTime, requestInConsideration );
        _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;


    }
}
