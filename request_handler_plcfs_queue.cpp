#include "request_handler_plcfs_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerPlcfsQueue )

//TODO fill in
RequestHandlerPlcfsQueue::RequestHandlerPlcfsQueue( Json::Value &config ) : RequestHandler( config ), _perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {

    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;
}

RequestHandlerPlcfsQueue::~RequestHandlerPlcfsQueue( ) {

}

void RequestHandlerPlcfsQueue::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {

    req->setRemainingWork( _perfModel );

    //todo: have to send it to front of the queue, and update the current job's completion time
    if ( _requestsInPLCFSQueue.size( ) == 0 ) {
        _requestsInPLCFSQueue.push_back( req );
    } else {
        Request *currentRunningRequest = _requestsInPLCFSQueue[ 0 ];

        uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
        uint64_t remainingWorkOfCurrentJob = currentRunningRequest->getRemainingWork( ) - doneTimeOfCurrentJob;
//        std::cout << "simtime: " << simulator::getSimTime( ) << "  starttime: " << _startTimeOfCurrentJob
//                  << " remaining: " << remainingWorkOfCurrentJob << "\n";
        currentRunningRequest->setRemainingWork( remainingWorkOfCurrentJob );

        _requestsInPLCFSQueue.insert( _requestsInPLCFSQueue.begin( ), req );


    }


    _startTimeOfCurrentJob=simulator::getSimTime();


    _remainingTimeOfJobs += req->getRemainingWork( );

    //todo: calculate completion time for incoming job
    uint64_t completionTime = simulator::getSimTime( ) + req->getRemainingWork( );

    //todo: map eventReference to Req
    simulator::EventReference eventReference = addCompletionEvent( completionTime, req );

    _mappingOfEventReferenceToRequests[ req ] = eventReference;


    _mappingOfCallbacksToRequests[ req ] = completionCallback;

    //todo: modify completionTime for previous jobs accordingly
    _modifyCompletionTimeForFollowingJobs( completionTime );


}

void RequestHandlerPlcfsQueue::notifyEnd( Request *req ) {

    _remainingTimeOfJobs -= req->getRemainingWork( );

    auto it = std::find( _requestsInPLCFSQueue.begin( ), _requestsInPLCFSQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != _requestsInPLCFSQueue.end( )) {
        indexOfRequest = static_cast<int>(std::distance( _requestsInPLCFSQueue.begin( ), it ));
    }
    if ( indexOfRequest == -1 ) {
        _remainingTimeOfJobs += req->getRemainingWork( );
        return;
    }

    _requestsInPLCFSQueue.erase( _requestsInPLCFSQueue.begin( ) + indexOfRequest );


    _startTimeOfCurrentJob = simulator::getSimTime( );

    auto itr = _mappingOfCallbacksToRequests.find( req );
    if ( itr != _mappingOfCallbacksToRequests.end( )) {
        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;
        _mappingOfCallbacksToRequests.erase( req );
        notifyListenersEnd( req );
        completionCallBackToBeRemoved->notifyEnd( req );
    }


    auto itr2 = _mappingOfEventReferenceToRequests.find( req );
    if ( itr2 != _mappingOfEventReferenceToRequests.end( )) {
        _mappingOfEventReferenceToRequests.erase( itr2 );
    }


    return;


}

unsigned int RequestHandlerPlcfsQueue::getQueueLength( ) {

    return ( unsigned int ) _requestsInPLCFSQueue.size( );
}

uint64_t RequestHandlerPlcfsQueue::getRemainingWorkLeft( ) {

    if ( _requestsInPLCFSQueue.size( ) == 0 ) {
        return 0;
    }
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
    return _remainingTimeOfJobs - doneTimeOfCurrentJob;

}

void RequestHandlerPlcfsQueue::_modifyCompletionTimeForFollowingJobs( uint64_t completionTime ) {

//    std::cout << "currently running: " << _requestsInPLCFSQueue[ 0 ]->getArrivalTime( ) << " - "
//              << _requestsInPLCFSQueue[ 0 ]->getRemainingWork( ) << " * " << _requestsInPLCFSQueue[ 0 ]->getSize( )
//              << " % " << completionTime << "\n";
    for ( unsigned long i = 1 ; i < _requestsInPLCFSQueue.size( ) ; i++ ) {

        Request *requestInConsideration = _requestsInPLCFSQueue[ i ];

        auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );
        if ( itr != _mappingOfEventReferenceToRequests.end( )) {
            simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
            _mappingOfEventReferenceToRequests.erase( itr );
        }

        completionTime += requestInConsideration->getRemainingWork( );


        simulator::EventReference newEventReference = addCompletionEvent( completionTime, requestInConsideration );
        _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;
//        std::cout << _requestsInPLCFSQueue[ i ]->getArrivalTime( ) << " - "
//                  << _requestsInPLCFSQueue[ i ]->getRemainingWork( ) << " * " << _requestsInPLCFSQueue[ i ]->getSize( )
//                  << " % " << completionTime << "\n";
    }

//    std::cout << "\n\n";
}