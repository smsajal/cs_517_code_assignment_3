#ifndef REQUEST_HANDLER_PLCFS_QUEUE_H
#define REQUEST_HANDLER_PLCFS_QUEUE_H

#include <vector>
#include <map>


#include "request_handler.h"
#include "request.h"
#include "listener.h"
#include "json/json.h"
#include "perf_model.h"
#include "simulator.h"

class RequestHandlerPlcfsQueue : public RequestHandler
{
private:
    //TODO fill in
    std::vector<Request*> _requestsInPLCFSQueue;
    std::map<Request*, ListenerEnd<Request*>* > _mappingOfCallbacksToRequests;
    std::map<Request*, simulator::EventReference > _mappingOfEventReferenceToRequests;

    uint64_t _startTimeOfCurrentJob;
    uint64_t _remainingTimeOfJobs;

    PerfModel *_perfModel;


    virtual void _modifyCompletionTimeForFollowingJobs(uint64_t completionTime);

public:
    RequestHandlerPlcfsQueue(Json::Value& config);
    virtual ~RequestHandlerPlcfsQueue();

    // Handle request sent to server
    virtual void handleRequest(Request* req, ListenerEnd<Request*>* completionCallback);

    // Called when request is complete
    virtual void notifyEnd(Request* req);

    // Get current queue length
    virtual unsigned int getQueueLength();

    // Get remaining work left in queue in nanoseconds
    virtual uint64_t getRemainingWorkLeft();
};

#endif /* REQUEST_HANDLER_PLCFS_QUEUE_H */
