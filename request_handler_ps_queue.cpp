#include "request_handler_ps_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerPsQueue )

//TODO fill in

RequestHandlerPsQueue::RequestHandlerPsQueue( Json::Value &config ) : RequestHandler( config ), _perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {

    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;
    _indexInserted = 0;
    _lastIncomingEventTime = 0;


}

RequestHandlerPsQueue::~RequestHandlerPsQueue( ) {

}

void RequestHandlerPsQueue::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {

//    std::cout << "\n\n\n\nincoming request:\narrive-time: " << req->getArrivalTime( ) << "  size: " << req->getSize( )
//              << "\n";
    req->setRemainingWork( _perfModel );
    //todo: insert request in the queue, should be done like srpt
//    _requestsInPSQueue.push_back( req );
    if ( _requestsInPSQueue.size( ) == 0 ) {
        _requestsInPSQueue.push_back( req );
        _indexInserted = 0;
    } else {
        _setIncomingRequestInCorrectPosition( req );
    }

    //fixme:  need to have _startTimeOfCurrentJob calculation (maybe)

    _remainingTimeOfJobs += req->getRemainingWork( );

    //todo: calculate completionTime for the request with smallest remaining work AND
    //todo: calculate completion time all the other jobs in the queue
    _modifyCompletionTimeForAllJobs( req );

    //todo: map eventReference to req
    //have been done in the _modifyCompletionTimeForAllJobs function

    //todo: map completionCallBack to req
    _mappingOfCallbacksToRequests[ req ] = completionCallback;

    if ( simulator::getSimTime( ) > _lastIncomingEventTime ) {
        _lastIncomingEventTime = simulator::getSimTime( );
    }

//    std::cout << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
//    for ( auto it = _mappingOfEventReferenceToRequests.begin( ) ;
//          it != _mappingOfEventReferenceToRequests.end( ) ; it++ ) {
//        std::cout << " arrival : " << it->first->getArrivalTime( ) << "  size: " << it->first->getSize( )
//                  << " completionTime: " << it->second->timestamp << "\n";
//
//    }

}

void RequestHandlerPsQueue::notifyEnd( Request *req ) {

    _remainingTimeOfJobs -= req->getRemainingWork( );

//    std::cout<<"######################################\n";
//    std::cout<<"remove arrivaltime: "<<req->getArrivalTime()<<" size: "<<req->getSize()<<" simtime: "<<simulator::getSimTime()<<"\n";
    //todo: remove the req from queue, and mappings
    auto it = std::find( _requestsInPSQueue.begin( ), _requestsInPSQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != _requestsInPSQueue.end( )) {

        indexOfRequest = static_cast<int>(std::distance( _requestsInPSQueue.begin( ), it ));

    }

    if ( indexOfRequest == -1 ) {
        //todo: return
        _remainingTimeOfJobs += req->getRemainingWork( );

        return;
    } else {
        //todo: update _remainingTimeOfJobs
    }

    _requestsInPSQueue.erase( _requestsInPSQueue.begin( ) + indexOfRequest );

    auto itr = _mappingOfCallbacksToRequests.find( req );
    if ( itr != _mappingOfCallbacksToRequests.end( )) {
        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;

        _mappingOfCallbacksToRequests.erase( itr );

        notifyListenersEnd( req );

        completionCallBackToBeRemoved->notifyEnd( req );
    }

    auto itr2 = _mappingOfEventReferenceToRequests.find( req );
    if ( itr2 != _mappingOfEventReferenceToRequests.end( )) {
        _mappingOfEventReferenceToRequests.erase( itr2 );
    }

    //todo: update completionTime for all the remaining jobs in the queue---- MAYBE LATER

}

unsigned int RequestHandlerPsQueue::getQueueLength( ) {
    return ( unsigned int ) _requestsInPSQueue.size( );
}

uint64_t RequestHandlerPsQueue::getRemainingWorkLeft( ) {

    if ( _requestsInPSQueue.size( )) {
        return 0;
    }

    //todo: this has to be changed....
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
    return _remainingTimeOfJobs - doneTimeOfCurrentJob;

}


//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************


void RequestHandlerPsQueue::_setIncomingRequestInCorrectPosition( Request *req ) {

    if ( _lastIncomingEventTime < simulator::getSimTime( )) {

        uint64_t doneWorkForEachReq = (simulator::getSimTime( ) - _lastIncomingEventTime) / _requestsInPSQueue.size( );

        for ( unsigned long i = 0 ; i < _requestsInPSQueue.size( ) ; i++ ) {

            Request *requestInConsideration = _requestsInPSQueue[ i ];

            uint64_t oldCompletionTime = _mappingOfEventReferenceToRequests[ requestInConsideration ]->timestamp;


            auto iterator = _mappingOfEventReferenceToRequests.find( requestInConsideration );
            if ( iterator != _mappingOfEventReferenceToRequests.end( )) {
                simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
                _mappingOfEventReferenceToRequests.erase( iterator );
//                std::cout << "removing!!!!!!\n";
            }
            uint64_t remainingWorkOfRequestInConsideration =
                    requestInConsideration->getRemainingWork( ) - doneWorkForEachReq;
            requestInConsideration->setRemainingWork( remainingWorkOfRequestInConsideration );

//            std::cout << "~~~~~adding event reference: \n";
//            std::cout << "arrival time " << requestInConsideration->getArrivalTime( ) << "  size "
//                      << requestInConsideration->getSize( ) << " completion time " << oldCompletionTime << "\n";
            simulator::EventReference newEventReference = addCompletionEvent( oldCompletionTime,
                                                                              requestInConsideration );
            _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;

        }
//        _lastIncomingEventTime=simulator::getSimTime();
    }
    _lastIncomingEventTime = simulator::getSimTime( );

    if ( req->getRemainingWork( ) < _requestsInPSQueue[ 0 ]->getRemainingWork( )) {
        _requestsInPSQueue.insert( _requestsInPSQueue.begin( ), req );

        _indexInserted = 0;
    } else {

        std::vector < Request * > v2( _requestsInPSQueue );
//        unsigned long priorCount = 0;

        v2.insert( v2.begin( ), req );
        std::sort( v2.begin( ), v2.end( ), _compareRequests );

        auto it = std::find( v2.begin( ), v2.end( ), req );
        int idx = -1;
        if ( it != v2.end( )) {
            idx = static_cast<int>(std::distance( v2.begin( ), it ));
        }
        if ( idx != -1 ) {
            _requestsInPSQueue.insert( _requestsInPSQueue.begin( ) + idx, req );
            _indexInserted = static_cast<unsigned long>(idx);
        }
    }
    return;

}

void RequestHandlerPsQueue::_modifyCompletionTimeForAllJobs( Request *req ) {

//todo: set _completionTimeOfMostRecentJob to be the completionTime of req
    std::map < Request *, uint64_t > mappingOfTemporaryRemainingTimeToRequests;

    for ( unsigned long i = 0 ; i < _requestsInPSQueue.size( ) ; i++ ) {
        Request *request = _requestsInPSQueue[ i ];
        mappingOfTemporaryRemainingTimeToRequests[ request ] = request->getRemainingWork( );
    }

//    std::cout << "----------------------------------------------\n";
    unsigned long jobCountInQueue = _requestsInPSQueue.size( );
    uint64_t newCompletionTime = simulator::getSimTime( );

    for ( unsigned long i = 0 ; i < _requestsInPSQueue.size( ) ; i++, jobCountInQueue-- ) {

        Request *requestInConsideration = _requestsInPSQueue[ i ];
        auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );

        if ( itr != _mappingOfEventReferenceToRequests.end( )) {

            simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
            _mappingOfEventReferenceToRequests.erase( itr );

        }

        newCompletionTime += (mappingOfTemporaryRemainingTimeToRequests[ requestInConsideration ] * jobCountInQueue);


        simulator::EventReference newEventReference = addCompletionEvent( newCompletionTime, requestInConsideration );
        _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;

        for ( unsigned long j = i + 1 ; j < _requestsInPSQueue.size( ) ; j++ ) {
            mappingOfTemporaryRemainingTimeToRequests[ _requestsInPSQueue[ j ]] =
                    _requestsInPSQueue[ j ]->getRemainingWork( ) - requestInConsideration->getRemainingWork( );
        }


        if ( _isTwoRequestsEqual( requestInConsideration, req )) {

            _completionTimeOfReq = newCompletionTime;
        }


    }


}

bool RequestHandlerPsQueue::_isTwoRequestsEqual( Request *req1, Request *req2 ) {

    bool arrivalTimeEqual = (req1->getArrivalTime( ) == req2->getArrivalTime( ));
    bool sizeEqual = (req1->getSize( ) == req2->getSize( ));

    return (arrivalTimeEqual && sizeEqual);
}

