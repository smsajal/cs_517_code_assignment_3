#ifndef REQUEST_HANDLER_PS_QUEUE_H
#define REQUEST_HANDLER_PS_QUEUE_H

#include "request_handler.h"
#include "request.h"
#include "listener.h"
#include "json/json.h"
#include "perf_model.h"
#include "simulator.h"

class RequestHandlerPsQueue : public RequestHandler {
private:
    //TODO fill in
    //todo: basically, this will be srpt, where the completionTime is calculated differently, and updated very frequently
    std::vector < Request * > _requestsInPSQueue;
    std::map < Request *, ListenerEnd < Request * > * > _mappingOfCallbacksToRequests;
    std::map < Request *, simulator::EventReference > _mappingOfEventReferenceToRequests;
//    std::map < Request *, uint64_t  > _mappingOfTemporaryRemainingTimeToRequests;

    uint64_t _startTimeOfCurrentJob;
    uint64_t _remainingTimeOfJobs;
    unsigned long _indexInserted;

    uint64_t _lastIncomingEventTime;
    PerfModel *_perfModel;

    uint64_t _completionTimeOfReq;

    virtual void _setIncomingRequestInCorrectPosition( Request *req );

    virtual void _modifyCompletionTimeForAllJobs( Request *req );

    virtual bool _isTwoRequestsEqual(Request *req1, Request *req2);

    static bool _compareRequests( Request *req1, Request *req2 ) {
        return (req1->getRemainingWork( ) < req2->getRemainingWork( ));
    }


public:
    RequestHandlerPsQueue( Json::Value &config );

    virtual ~RequestHandlerPsQueue( );

    // Handle request sent to server
    virtual void handleRequest( Request *req, ListenerEnd < Request * > *completionCallback );

    // Called when request is complete
    virtual void notifyEnd( Request *req );

    // Get current queue length
    virtual unsigned int getQueueLength( );

    // Get remaining work left in queue in nanoseconds
    virtual uint64_t getRemainingWorkLeft( );
};

#endif /* REQUEST_HANDLER_PS_QUEUE_H */
