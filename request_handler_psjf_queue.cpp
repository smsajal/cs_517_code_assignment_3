#include "request_handler_psjf_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerPsjfQueue )

//TODO fill in

RequestHandlerPsjfQueue::RequestHandlerPsjfQueue( Json::Value &config ) : RequestHandler( config ), _perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {

    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;

}

RequestHandlerPsjfQueue::~RequestHandlerPsjfQueue( ) {

}

void RequestHandlerPsjfQueue::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {

    req->setRemainingWork( _perfModel );

    //todo: set request in correct place in the queue
    if ( _requestsInPSJFQueue.size( ) == 0 ) {
        _requestsInPSJFQueue.push_back( req );
        _indexInserted = 0;
    } else {
        _setIncomingRequestInCorrectPosition( req );
    }

    //todo: update startTimeOfCurrentJob
    if ( _requestsInPSJFQueue.size( ) == 1 ) {
        _startTimeOfCurrentJob = simulator::getSimTime( );
    }
    _remainingTimeOfJobs += req->getRemainingWork( );
    //todo: calculate completion time for the req
    uint64_t completionTime;
    if ( _requestsInPSJFQueue.size( ) == 1 ) {
        completionTime = _startTimeOfCurrentJob + req->getRemainingWork( );
    } else {
        uint64_t remainingTimeOfCurrentJob =
                _startTimeOfCurrentJob + _requestsInPSJFQueue[ 0 ]->getRemainingWork( ) - simulator::getSimTime( );

        if ( _indexInserted == 0 ) {
            completionTime = remainingTimeOfCurrentJob + simulator::getSimTime( );
        } else {
            completionTime = remainingTimeOfCurrentJob + req->getRemainingWork( ) + simulator::getSimTime( );

        }
    }

    //todo: map eventReference to req
    simulator::EventReference eventReference = addCompletionEvent( completionTime, req );
    _mappingOfEventReferenceToRequests[ req ] = eventReference;


    _mappingOfCallbacksToRequests[ req ] = completionCallback;

    //todo: modify comletionTimes of Jobs behind this
    _modifyCompletionTimeForFollowingJobs( completionTime );


}

void RequestHandlerPsjfQueue::notifyEnd( Request *req ) {

    _remainingTimeOfJobs -= req->getRemainingWork( );

    auto it = std::find( _requestsInPSJFQueue.begin( ), _requestsInPSJFQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != _requestsInPSJFQueue.end( )) {
        indexOfRequest = static_cast<int>(std::distance( _requestsInPSJFQueue.begin( ), it ));
    }
    if ( indexOfRequest == -1 ) {
        _remainingTimeOfJobs += req->getRemainingWork( );
        return;
    }

    _requestsInPSJFQueue.erase( _requestsInPSJFQueue.begin( ) + indexOfRequest );

    _startTimeOfCurrentJob = simulator::getSimTime( );

    auto itr = _mappingOfCallbacksToRequests.find( req );
    if ( itr != _mappingOfCallbacksToRequests.end( )) {
        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;
        _mappingOfCallbacksToRequests.erase( itr );
        notifyListenersEnd( req );
        completionCallBackToBeRemoved->notifyEnd( req );
    }

    auto itr2 = _mappingOfEventReferenceToRequests.find( req );
    if ( itr2 != _mappingOfEventReferenceToRequests.end( )) {
        _mappingOfEventReferenceToRequests.erase( itr2 );
    }


    return;
}

unsigned int RequestHandlerPsjfQueue::getQueueLength( ) {
    return ( unsigned int ) _requestsInPSJFQueue.size( );
}

uint64_t RequestHandlerPsjfQueue::getRemainingWorkLeft( ) {

    if ( _requestsInPSJFQueue.size( ) == 0 ) {
        return 0;
    }
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
    return _remainingTimeOfJobs - doneTimeOfCurrentJob;
}


//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************


void RequestHandlerPsjfQueue::_setIncomingRequestInCorrectPosition( Request *req ) {

    if ( req->getSize( ) < _requestsInPSJFQueue[ 0 ]->getSize( )) {
        Request *currentRunningRequest = _requestsInPSJFQueue[ 0 ];
        uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
        uint64_t remainingWorkOfCurrentJob = currentRunningRequest->getRemainingWork( ) - doneTimeOfCurrentJob;
        currentRunningRequest->setRemainingWork( remainingWorkOfCurrentJob );


        _requestsInPSJFQueue.insert( _requestsInPSJFQueue.begin( ), req );
        _startTimeOfCurrentJob = simulator::getSimTime( );

        _indexInserted = 0;
    } else {

        std::vector < Request * > v2( _requestsInPSJFQueue );
        unsigned long priorCount = 0;

        if ( simulator::getSimTime( ) > v2[ 0 ]->getArrivalTime( )) {
            v2.erase( v2.begin( ));
            priorCount++;
        }

        v2.insert( v2.begin( ), req );
        std::sort( v2.begin( ), v2.end( ), _compareRequests );

        auto it = std::find( v2.begin( ), v2.end( ), req );
        int idx = -1;
        if ( it != v2.end( )) {
            idx = static_cast<int>(std::distance( v2.begin( ), it ));
        }

        if ( idx != -1 ) {
            _requestsInPSJFQueue.insert( _requestsInPSJFQueue.begin( ) + idx + static_cast<int>(priorCount), req );
            _indexInserted = static_cast<unsigned long>(idx) + priorCount;

        }
    }
    return;


}

void RequestHandlerPsjfQueue::_modifyCompletionTimeForFollowingJobs( uint64_t completionTime ) {

    for ( unsigned long i = _indexInserted + 1 ; i < _requestsInPSJFQueue.size( ) ; i++ ) {
        Request *requestInConsideration = _requestsInPSJFQueue[ i ];

        auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );
        if ( itr != _mappingOfEventReferenceToRequests.end( )) {

            simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
            _mappingOfEventReferenceToRequests.erase( itr );
        }

        completionTime += requestInConsideration->getRemainingWork( );

        simulator::EventReference newEventReference = addCompletionEvent( completionTime, requestInConsideration );
        _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;

    }

//    for ( unsigned long i = 1 ; i < _requestsInPSJFQueue.size( ) ; i++ ) {
//
//        std::cout << _requestsInPSJFQueue[ i ]->getArrivalTime( ) << " - "
//                  << _requestsInPSJFQueue[ i ]->getRemainingWork( ) << " * " << _requestsInPSJFQueue[ i ]->getSize( )
//                  << " % " << completionTime << "\n";
//    }
}