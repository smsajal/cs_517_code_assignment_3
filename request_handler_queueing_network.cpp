#include "request_handler_queueing_network.h"
#include "request.h"
#include "random_helper.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerQueueingNetwork )

//TODO fill in
RequestHandlerQueueingNetwork::RequestHandlerQueueingNetwork ( Json::Value &config )
        : RequestHandler ( config ),
          _config ( config ) {


}

RequestHandlerQueueingNetwork::~RequestHandlerQueueingNetwork ( ) {


}

void RequestHandlerQueueingNetwork::populateInitialProbabilities ( ) {
    //************************ populate the initial transition probabilities
    Json::Value &initialTransitionProbabilities = _config[ "initialTransitionProbability" ];
    for ( int i = 0; i < initialTransitionProbabilities.size ( ); i++ ) {

        //converting returned char* to std::string
        std::string queue = initialTransitionProbabilities[ i ][ "queue" ].asString ( );

        //converting returned double* to double
        double probability = initialTransitionProbabilities[ i ][ "probability" ].asDouble ( );

        _initialTransitionProbabilities[ queue ] = probability;


    }

    return;
}

void RequestHandlerQueueingNetwork::populateTransitionProbabilities ( ) {

    //************************ populate the transition probabilities
    Json::Value &transitionProbabilities = _config[ "transitionProbabilities" ];

    for ( int i = 0; i < transitionProbabilities.size ( ); i++ ) {
        std::string presentQueue = transitionProbabilities[ i ][ "queue" ].asString ( );

        std::map<std::string, double> transitionProbability;
        double cumulativeProbability = 0;
        for ( int j = 0; j < transitionProbabilities[ i ][ "transitionProbability" ].size ( ); j++ ) {

            std::string queueToGo = transitionProbabilities[ i ][ "transitionProbability" ][ j ][ "queue" ].asString ( );
            double probability = transitionProbabilities[ i ][ "transitionProbability" ][ j ][ "probability" ].asDouble ( );
            transitionProbability[ queueToGo ] = probability;
            cumulativeProbability += probability;

        }

        if ( cumulativeProbability < 1.0 ) {
            double exitProbability = 1.0 - cumulativeProbability;
            std::string exitState = "exitState";
            transitionProbability[ exitState ] = exitProbability;
        }

        _transitionProbabilities[ presentQueue ] = transitionProbability;
    }

    return;
}

void RequestHandlerQueueingNetwork::init ( ) {

    populateInitialProbabilities ( );

    populateTransitionProbabilities ( );

    // _currentQueueName = "";

    return;

}


std::string RequestHandlerQueueingNetwork::getNextQueueName ( double coinTossResult,
                                                              std::map<std::string, double> transitionProbabilities ) {

    std::vector<double> cumulativeProbabilities;
    std::vector<std::string> queueLabels;
    double cumulativeProbabilityValue = 0;


    for ( auto it = transitionProbabilities.begin ( ); it != transitionProbabilities.end ( ); it++ ) {

        cumulativeProbabilityValue += it->second;

        cumulativeProbabilities.push_back ( cumulativeProbabilityValue );
        queueLabels.push_back ( it->first );

    }


    std::string queueName = "";

    for ( unsigned int i = 0; i < cumulativeProbabilities.size ( ); i++ ) {

        if ( coinTossResult <= cumulativeProbabilities[ i ] ) {
            //get the queue-name here
            queueName = queueLabels[ i ];
            break;
        }
    }

    return queueName;

}

void RequestHandlerQueueingNetwork::handleRequest ( Request *req, ListenerEnd<Request *> *completionCallback ) {

    double coinTossResult = uniform01 ( );

    //_currentQueueName = getNextQueueName ( coinTossResult, _initialTransitionProbabilities );
    _mappingOfCurrentStateToRequest[ req ] = getNextQueueName ( coinTossResult, _initialTransitionProbabilities );

    _mappingOfCallbacksToRequests[ req ] = completionCallback;


    RequestHandler *requestHandler = Factory<RequestHandler>::getObjectByName (
            _mappingOfCurrentStateToRequest[ req ] );
    requestHandler->handleRequest ( req, this );


    return;


}

void RequestHandlerQueueingNetwork::releaseRequestFromQueue ( Request *req ) {

    auto it = _mappingOfCallbacksToRequests.find ( req );

    ListenerEnd<Request *> *completionCallbackToBeRemoved = it->second;

    _mappingOfCallbacksToRequests.erase ( it );


    _mappingOfCurrentStateToRequest.erase(req);


    completionCallbackToBeRemoved->notifyEnd ( req );



    return;
}

void RequestHandlerQueueingNetwork::notifyEnd ( Request *req ) {


    double coinTossResult = uniform01 ( );

    auto it = _transitionProbabilities.find ( _mappingOfCurrentStateToRequest[req] );

    if ( it != _transitionProbabilities.end ( )) {


        // perform non-exit here....
        std::string nextQueueName = getNextQueueName ( coinTossResult,
                                                       _transitionProbabilities[ _mappingOfCurrentStateToRequest[ req ]] );
        if ( nextQueueName == "exitState" ) {
            releaseRequestFromQueue ( req );
        } else {
            _mappingOfCurrentStateToRequest[ req ] = nextQueueName;
            RequestHandler *requestHandler = Factory<RequestHandler>::getObjectByName (
                    _mappingOfCurrentStateToRequest[ req ] );
            requestHandler->handleRequest ( req, this );

        }


    } else {
        //perform exit here....
        releaseRequestFromQueue ( req );
    }


    return;


}