#ifndef REQUEST_HANDLER_QUEUEING_NETWORK_H
#define REQUEST_HANDLER_QUEUEING_NETWORK_H

#include <unordered_map>
#include <map>
#include "request_handler.h"
#include "request.h"
#include "listener.h"
#include "json/json.h"

class RequestHandlerQueueingNetwork : public RequestHandler {
private:
    //TODO fill in
    Json::Value _config;

    std::map<std::string, double> _initialTransitionProbabilities;

    std::unordered_map<std::string, std::map<std::string, double> > _transitionProbabilities;

    std::map<Request*, ListenerEnd<Request *> *> _mappingOfCallbacksToRequests;

    std::map<Request*, std::string> _mappingOfCurrentStateToRequest;
//    ListenerEnd<Request *> *_completionCallback;
public:
    RequestHandlerQueueingNetwork ( Json::Value &config );

    virtual ~RequestHandlerQueueingNetwork ( );

    virtual void init ( );


    // Handle request sent to server
    virtual void handleRequest ( Request *req, ListenerEnd<Request *> *completionCallback );

    // Called when request is complete
    virtual void notifyEnd ( Request *req );

    //functions written by me
    virtual void populateInitialProbabilities();
    virtual void populateTransitionProbabilities();
    virtual void releaseRequestFromQueue(Request *req);
    virtual std::string getNextQueueName(double coinTossResult,std::map<std::string, double> transitionProbabilities);

};

#endif /* REQUEST_HANDLER_QUEUEING_NETWORK_H */
