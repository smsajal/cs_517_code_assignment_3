#include "request_handler_random_queue.h"
#include "request.h"
#include "listener.h"
#include "random_helper.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerRandomQueue )

//TODO fill in
RequestHandlerRandomQueue::RequestHandlerRandomQueue( Json::Value &config ) : RequestHandler( config ), perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {

    startTimeOfCurrentJob = 0;
    remainingTimeOfJobs = 0;

}

RequestHandlerRandomQueue::~RequestHandlerRandomQueue( ) {

}

void RequestHandlerRandomQueue::handleRequest( Request *req,
                                               ListenerEnd < Request * > *completionCallback ) {

    req->setRemainingWork( perfModel );

    requestsInRandomQueue.push_back( req );

    //todo: randomize the queue here
    unsigned seed = static_cast<unsigned >(std::chrono::system_clock::now( ).time_since_epoch( ).count( ));
    std::shuffle( requestsInRandomQueue.begin( ), requestsInRandomQueue.end( ), std::default_random_engine( seed ));


    if ( requestsInRandomQueue.size( ) == 1 ) {

        startTimeOfCurrentJob = simulator::getSimTime( );

    }

    remainingTimeOfJobs += req->getRemainingWork( );

    //todo: calculate completion time for the req who has come, the one commented here is from fifo
    uint64_t completionTime;
    completionTime =
            getRemainingWorkLeft( ) + simulator::getSimTime( );


    addCompletionEvent( completionTime, req );

    mappingOfCallbacksToRequests[ req ] = completionCallback;

    return;


}

void RequestHandlerRandomQueue::notifyEnd( Request *req ) {

    remainingTimeOfJobs -= req->getRemainingWork( );

    //requestsInRandomQueue.pop( );
    //todo: do something equivalent to this for vector
    auto it = std::find( requestsInRandomQueue.begin( ), requestsInRandomQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != requestsInRandomQueue.end( )) {

        indexOfRequest = static_cast<int>(std::distance( requestsInRandomQueue.begin( ), it ));
    }
    if ( indexOfRequest == -1 ) {
        std::cout << "Request Not Found\n";
        remainingTimeOfJobs += req->getRemainingWork( );
        return;
    }
    requestsInRandomQueue.erase( requestsInRandomQueue.begin( ) + indexOfRequest );
    //todo: vector pop of the request is done.....




    startTimeOfCurrentJob = simulator::getSimTime( );


    auto itr = mappingOfCallbacksToRequests.find( req );

    if ( itr != mappingOfCallbacksToRequests.end( )) {

        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;
        mappingOfCallbacksToRequests.erase( itr );

        notifyListenersEnd( req );

        completionCallBackToBeRemoved->notifyEnd( req );

    }

    return;
}

unsigned int RequestHandlerRandomQueue::getQueueLength( ) {

    return ( unsigned int ) requestsInRandomQueue.size( );

}

uint64_t RequestHandlerRandomQueue::getRemainingWorkLeft( ) {

    if ( requestsInRandomQueue.size( ) == 0 ) {
        return 0;
    }
    //todo: modify the code here to get remaining work..... (maybe????)
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - startTimeOfCurrentJob;
    return remainingTimeOfJobs - doneTimeOfCurrentJob;
}

//uint64_t RequestHandlerRandomQueue::getCompletionTimeInRandomQueue( Request *req ) {
//
//    auto it = std::find( requestsInRandomQueue.begin( ), requestsInRandomQueue.end( ), req );
//    int indexOfRequest = 0;
//    if ( it != requestsInRandomQueue.cend( )) {
//
//        indexOfRequest = std::distance( requestsInRandomQueue.begin( ), it );
//    }
//    if ( indexOfRequest == 0 ) {
//        std::cout << "Request Not Found\n";
//    }
//
//
//    uint64_t workLeftInSystemUptoRequest = 0;
//    //idx starts from 1, coz 0th req is in process.....
//    for ( int i = 1 ; i <= indexOfRequest ; i++ ) {
//
//        workLeftInSystemUptoRequest += requestsInRandomQueue[ i ]->getRemainingWork( );
//    }
//    // considering the remaining work of current job
//    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - startTimeOfCurrentJob;
//    uint64_t remainingTimeOfCurrentJob = requestsInRandomQueue[ 0 ]->getRemainingWork( ) - doneTimeOfCurrentJob;
//
//    uint64_t completionTimeOfRequest =
//            remainingTimeOfCurrentJob + workLeftInSystemUptoRequest + simulator::getSimTime( );
//
//    return completionTimeOfRequest;
//}