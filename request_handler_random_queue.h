#ifndef REQUEST_HANDLER_RANDOM_QUEUE_H
#define REQUEST_HANDLER_RANDOM_QUEUE_H

#include <queue>
#include <map>
#include <random>
#include <chrono>


#include "request_handler.h"
#include "request.h"
#include "listener.h"
#include "json/json.h"
#include "perf_model.h"

class RequestHandlerRandomQueue : public RequestHandler {
private:
    //TODO fill in
    std::vector < Request * > requestsInRandomQueue;
    std::map < Request *, ListenerEnd < Request * > * > mappingOfCallbacksToRequests;

    uint64_t startTimeOfCurrentJob;
    uint64_t remainingTimeOfJobs;

    PerfModel *perfModel;

public:
    RequestHandlerRandomQueue( Json::Value &config );

    virtual ~RequestHandlerRandomQueue( );

    // Handle request sent to server
    virtual void handleRequest( Request *req, ListenerEnd < Request * > *completionCallback );

    // Called when request is complete
    virtual void notifyEnd( Request *req );

    // Get current queue length
    virtual unsigned int getQueueLength( );

    // Get remaining work left in queue in nanoseconds
    virtual uint64_t getRemainingWorkLeft( );


    //todo: extra functions written
//    virtual uint64_t getCompletionTimeInRandomQueue(Request *req);
};

#endif /* REQUEST_HANDLER_RANDOM_QUEUE_H */
