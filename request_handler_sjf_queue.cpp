#include "request_handler_sjf_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerSjfQueue )

//TODO fill in
RequestHandlerSjfQueue::RequestHandlerSjfQueue( Json::Value &config ) : RequestHandler( config ), _perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {

    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;

}

RequestHandlerSjfQueue::~RequestHandlerSjfQueue( ) {

}

void RequestHandlerSjfQueue::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {

    req->setRemainingWork( _perfModel );
    //todo: set the req in its correct place in the queue
    if ( _requestsInSJFQueue.size( ) == 0 ) {
        _requestsInSJFQueue.push_back( req );
    } else {
        _setIncomingRequestInCorrectPosition( req );
    }

    if ( _requestsInSJFQueue.size( ) == 1 ) {
        _startTimeOfCurrentJob = simulator::getSimTime( );
    }
    _remainingTimeOfJobs += req->getRemainingWork( );

    //todo: calculate completion time for the req
    uint64_t completionTime;
    if ( _requestsInSJFQueue.size( ) == 1 ) {
        completionTime = _startTimeOfCurrentJob + req->getRemainingWork( );
    } else {
        uint64_t remainingTimeOfCurrentJob =
                _startTimeOfCurrentJob + _requestsInSJFQueue[ 0 ]->getRemainingWork( ) - simulator::getSimTime( );
        completionTime = remainingTimeOfCurrentJob + req->getRemainingWork( ) + simulator::getSimTime( );
    }

    //todo: map eventReference to req
//    std::cout << "outside loop: \n";
//    std::cout << "---arrival: " << req->getArrivalTime( ) << " size: " << req->getSize( ) << " completionTime: "
//              << completionTime << "\n";
    simulator::EventReference eventReference = addCompletionEvent( completionTime, req );
    _mappingOfEventReferenceToRequests[ req ] = eventReference;

    _mappingOfCallbacksToRequests[ req ] = completionCallback;


    //todo: modify completion time for jobs greater than req
    _modifyCompletionTimeForFollowingJobs( completionTime );
}

void RequestHandlerSjfQueue::notifyEnd( Request *req ) {

    _remainingTimeOfJobs -= req->getRemainingWork( );

    auto it = std::find( _requestsInSJFQueue.begin( ), _requestsInSJFQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != _requestsInSJFQueue.end( )) {
        indexOfRequest = static_cast<int>(std::distance( _requestsInSJFQueue.begin( ), it ));

    }

    if ( indexOfRequest == -1 ) {
        _remainingTimeOfJobs += req->getRemainingWork( );
        return;
    }

    _requestsInSJFQueue.erase( _requestsInSJFQueue.begin( ) + indexOfRequest );


    _startTimeOfCurrentJob = simulator::getSimTime( );

    auto itr = _mappingOfCallbacksToRequests.find( req );
    if ( itr != _mappingOfCallbacksToRequests.end( )) {
        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;

        _mappingOfCallbacksToRequests.erase( itr );
        notifyListenersEnd( req );

        completionCallBackToBeRemoved->notifyEnd( req );
    }


    auto itr2 = _mappingOfEventReferenceToRequests.find( req );
    if ( itr2 != _mappingOfEventReferenceToRequests.end( )) {
        _mappingOfEventReferenceToRequests.erase( itr2 );
    }


//    std::cout << "\n\n\n\n\n";
    return;

}

unsigned int RequestHandlerSjfQueue::getQueueLength( ) {

    return ( unsigned int ) _requestsInSJFQueue.size( );

}

uint64_t RequestHandlerSjfQueue::getRemainingWorkLeft( ) {

    if ( _requestsInSJFQueue.size( ) == 0 ) {
        return 0;
    }
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
    return _remainingTimeOfJobs - doneTimeOfCurrentJob;
}


//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

void RequestHandlerSjfQueue::_setIncomingRequestInCorrectPosition( Request *req ) {

    std::vector < Request * > v2( _requestsInSJFQueue );
    unsigned long priorCount = 0;
    if ( simulator::getSimTime( ) > v2[ 0 ]->getArrivalTime( )) {

        v2.erase( v2.begin( ));
        priorCount++;
    }


    v2.insert( v2.begin( ), req );
    std::sort( v2.begin( ), v2.end( ), _compareRequests );

    auto it = std::find( v2.begin( ), v2.end( ), req );
    int idx = -1;
    if ( it != v2.end( )) {

        idx = static_cast<int>(std::distance( v2.begin( ), it ));

    }


    if ( idx != -1 ) {
        _requestsInSJFQueue.insert( _requestsInSJFQueue.begin( ) + idx + static_cast<int>(priorCount), req );
        _indexInserted = static_cast<unsigned long>(idx) + priorCount;

        return;
    }


    return;
}

void RequestHandlerSjfQueue::_modifyCompletionTimeForFollowingJobs( uint64_t completionTime ) {

//    std::cout << "inside loop, i starts at:  " << _indexInserted + 1 << "\n";
    for ( unsigned long i = _indexInserted + 1 ; i < _requestsInSJFQueue.size( ) ; i++ ) {
        Request *requestInConsideration = _requestsInSJFQueue[ i ];

        auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );

        if ( itr != _mappingOfEventReferenceToRequests.end( )) {


            simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
            _mappingOfEventReferenceToRequests.erase( itr );

        }

        completionTime += requestInConsideration->getRemainingWork( );

//        std::cout << "****arrival: " << requestInConsideration->getArrivalTime( ) << " size: "
//                  << requestInConsideration->getSize( ) << " completionTime: " << completionTime << "\n";
        simulator::EventReference newEventReference = addCompletionEvent( completionTime, requestInConsideration );
        _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;


    }
}