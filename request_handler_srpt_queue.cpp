#include "request_handler_srpt_queue.h"
#include "request.h"
#include "listener.h"
#include "factory.h"

REGISTER_CLASS( RequestHandler, RequestHandlerSrptQueue )

//TODO fill in
RequestHandlerSrptQueue::RequestHandlerSrptQueue( Json::Value &config ) : RequestHandler( config ), _perfModel(
        Factory < PerfModel >::create( config[ "perfModel" ] )) {

    _startTimeOfCurrentJob = 0;
    _remainingTimeOfJobs = 0;
    _indexInserted = 0;
    _lastIncomingEventTime = 0;
}

RequestHandlerSrptQueue::~RequestHandlerSrptQueue( ) {

}

void RequestHandlerSrptQueue::handleRequest( Request *req, ListenerEnd < Request * > *completionCallback ) {


    req->setRemainingWork( _perfModel );
    //todo: set req in correct place in queue
    if ( _requestsInSRPTQueue.size( ) == 0 ) {
        _requestsInSRPTQueue.push_back( req );
        _indexInserted = 0;
    } else {
        _setIncomingRequestInCorrectPosition( req );
    }

    //todo: update startTimeOfCurrentJob
    if ( _requestsInSRPTQueue.size( ) == 1 ) {
        _startTimeOfCurrentJob = simulator::getSimTime( );
    }

    _remainingTimeOfJobs += req->getRemainingWork( );

    //todo: calculate completion time for req
    uint64_t completionTime;
    if ( _requestsInSRPTQueue.size( ) == 1 ) {
        completionTime = _startTimeOfCurrentJob + req->getRemainingWork( );
    } else {
//        uint64_t remainingTimeOfCurrentJob =
//                _startTimeOfCurrentJob + _requestsInSRPTQueue[ 0 ]->getRemainingWork( ) - simulator::getSimTime( );
        uint64_t remainingTimeOfCurrentJob = _requestsInSRPTQueue[ 0 ]->getRemainingWork( );
//        std::cout << "_startTimeOfCurrentJob = " << _startTimeOfCurrentJob << "  srpt[0]->remainingwork= "
//                  << _requestsInSRPTQueue[ 0 ]->getRemainingWork( ) << " simtime = " << simulator::getSimTime( )
//                  << "\n";
        if ( _indexInserted == 0 ) {
            completionTime = remainingTimeOfCurrentJob + simulator::getSimTime( );
        } else {
            completionTime = remainingTimeOfCurrentJob + req->getRemainingWork( ) + simulator::getSimTime( );
//            std::cout << "remainingTimeOfCurrentJob = " << remainingTimeOfCurrentJob << " req->remainingWork= "
//                      << req->getRemainingWork( ) << " simtime= " << simulator::getSimTime( ) << "\n";
        }
    }

    //todo: map eventReference to req
    simulator::EventReference eventReference = addCompletionEvent( completionTime, req );
    _mappingOfEventReferenceToRequests[ req ] = eventReference;

    _mappingOfCallbacksToRequests[ req ] = completionCallback;


    //todo: modify completionTimes of jobs behind this
    _modifyCompletionTimeForFollowingJobs( completionTime );

    if ( simulator::getSimTime( ) > _lastIncomingEventTime ) {
        _lastIncomingEventTime = simulator::getSimTime( );
    }

}

void RequestHandlerSrptQueue::notifyEnd( Request *req ) {

    _remainingTimeOfJobs -= req->getRemainingWork( );

    auto it = std::find( _requestsInSRPTQueue.begin( ), _requestsInSRPTQueue.end( ), req );
    int indexOfRequest = -1;
    if ( it != _requestsInSRPTQueue.end( )) {
        indexOfRequest = static_cast<int>(std::distance( _requestsInSRPTQueue.begin( ), it ));
    }

    if ( indexOfRequest == -1 ) {
        _remainingTimeOfJobs += req->getRemainingWork( );
        return;
    }

    _requestsInSRPTQueue.erase( _requestsInSRPTQueue.begin( ) + indexOfRequest );

    _startTimeOfCurrentJob = simulator::getSimTime( );

    auto itr = _mappingOfCallbacksToRequests.find( req );
    if ( itr != _mappingOfCallbacksToRequests.end( )) {
        ListenerEnd < Request * > *completionCallBackToBeRemoved = itr->second;

        _mappingOfCallbacksToRequests.erase( itr );
        notifyListenersEnd( req );

        completionCallBackToBeRemoved->notifyEnd( req );
    }

    auto itr2 = _mappingOfEventReferenceToRequests.find( req );
    if ( itr2 != _mappingOfEventReferenceToRequests.end( )) {
        _mappingOfEventReferenceToRequests.erase( itr2 );
    }

    return;
}

unsigned int RequestHandlerSrptQueue::getQueueLength( ) {
    return ( unsigned int ) _requestsInSRPTQueue.size( );
}

uint64_t RequestHandlerSrptQueue::getRemainingWorkLeft( ) {
    if ( _requestsInSRPTQueue.size( ) == 0 ) {
        return 0;
    }
    uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
    return _remainingTimeOfJobs - doneTimeOfCurrentJob;
}

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

//*********************************************************************************************

void RequestHandlerSrptQueue::_setIncomingRequestInCorrectPosition( Request *req ) {

    Request *currentRunningRequest = _requestsInSRPTQueue[ 0 ];

    if ( _lastIncomingEventTime < simulator::getSimTime( )) {
        uint64_t doneTimeOfCurrentJob = simulator::getSimTime( ) - _startTimeOfCurrentJob;
        uint64_t remainingWorkOfCurrentJob = currentRunningRequest->getRemainingWork( ) - doneTimeOfCurrentJob;
        currentRunningRequest->setRemainingWork( remainingWorkOfCurrentJob );
    }
    if ( req->getRemainingWork( ) < currentRunningRequest->getRemainingWork( )) {
        _requestsInSRPTQueue.insert( _requestsInSRPTQueue.begin( ), req );
        _startTimeOfCurrentJob = simulator::getSimTime( );

        _indexInserted = 0;
    } else {
        std::vector < Request * > v2( _requestsInSRPTQueue );
        unsigned long priorCount = 0;

        if ( simulator::getSimTime( ) > v2[ 0 ]->getArrivalTime( )) {
            v2.erase( v2.begin( ));
            priorCount++;
        }

        v2.insert( v2.begin( ), req );
        std::sort( v2.begin( ), v2.end( ), _compareRequests );

        auto it = std::find( v2.begin( ), v2.end( ), req );
        int idx = -1;
        if ( it != v2.end( )) {
            idx = static_cast<int>(std::distance( v2.begin( ), it ));
        }

        if ( idx != -1 ) {
            _requestsInSRPTQueue.insert( _requestsInSRPTQueue.begin( ) + idx + static_cast<int>(priorCount), req );
            _indexInserted = static_cast<unsigned long>(idx) + priorCount;
        }

    }
//    std::cout << "_indexInserted: " << _indexInserted << "\n";
    return;

}

void RequestHandlerSrptQueue::_modifyCompletionTimeForFollowingJobs( uint64_t completionTime ) {
//    std::cout << "################################################\n";
//    std::cout << "sim time: " << simulator::getSimTime( ) << "\n";
//    std::cout << "currently running: " << _requestsInSRPTQueue[ 0 ]->getArrivalTime( ) << " - "
//              << _requestsInSRPTQueue[ 0 ]->getSize( ) << " * " << _requestsInSRPTQueue[ 0 ]->getRemainingWork( )
//              << " % " << completionTime << "\n";

    for ( unsigned long i = _indexInserted + 1 ; i < _requestsInSRPTQueue.size( ) ; i++ ) {

        Request *requestInConsideration = _requestsInSRPTQueue[ i ];

        auto itr = _mappingOfEventReferenceToRequests.find( requestInConsideration );
        if ( itr != _mappingOfEventReferenceToRequests.end( )) {
            simulator::removeEvent( _mappingOfEventReferenceToRequests[ requestInConsideration ] );
            _mappingOfEventReferenceToRequests.erase( itr );
        }

        completionTime += requestInConsideration->getRemainingWork( );

        simulator::EventReference newEventReference = addCompletionEvent( completionTime, requestInConsideration );
        _mappingOfEventReferenceToRequests[ requestInConsideration ] = newEventReference;

    }

//    for ( unsigned long i = 1 ; i < _requestsInSRPTQueue.size( ) ; i++ ) {
//
//        std::cout << _requestsInSRPTQueue[ i ]->getArrivalTime( ) << " - "
//                  << _requestsInSRPTQueue[ i ]->getSize( ) << " * " << _requestsInSRPTQueue[ i ]->getRemainingWork( )
//                  << " % " << completionTime << "\n";
//    }
//
//    std::cout << "\n\n";
}
