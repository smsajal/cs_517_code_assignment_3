#ifndef REQUEST_HANDLER_SRPT_QUEUE_H
#define REQUEST_HANDLER_SRPT_QUEUE_H

#include <vector>
#include <map>
#include "request_handler.h"
#include "request.h"
#include "listener.h"
#include "json/json.h"
#include "perf_model.h"
#include "simulator.h"

class RequestHandlerSrptQueue : public RequestHandler {
private:
    //TODO fill in
    std::vector < Request * > _requestsInSRPTQueue;
    std::map < Request *, ListenerEnd < Request * > * > _mappingOfCallbacksToRequests;
    std::map < Request *, simulator::EventReference > _mappingOfEventReferenceToRequests;

    uint64_t _startTimeOfCurrentJob;
    uint64_t _remainingTimeOfJobs;
    unsigned long _indexInserted;

    uint64_t _lastIncomingEventTime;
    PerfModel *_perfModel;

    virtual void _setIncomingRequestInCorrectPosition( Request *req );

    virtual void _modifyCompletionTimeForFollowingJobs( uint64_t completionTime );

    static bool _compareRequests( Request *req1, Request *req2 ) {


        return (req1->getRemainingWork( ) < req2->getRemainingWork( ));

    }

public:
    RequestHandlerSrptQueue( Json::Value &config );

    virtual ~RequestHandlerSrptQueue( );

    // Handle request sent to server
    virtual void handleRequest( Request *req, ListenerEnd < Request * > *completionCallback );

    // Called when request is complete
    virtual void notifyEnd( Request *req );

    // Get current queue length
    virtual unsigned int getQueueLength( );

    // Get remaining work left in queue in nanoseconds
    virtual uint64_t getRemainingWorkLeft( );
};

#endif /* REQUEST_HANDLER_SRPT_QUEUE_H */
