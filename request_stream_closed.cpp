#include "request_stream_closed.h"
#include "factory.h"
#include "time_helper.h"

REGISTER_CLASS( RequestStream, RequestStreamClosed )

//TODO fill in
RequestStreamClosed::RequestStreamClosed ( Json::Value &config ) : RequestStream ( config ),
                                                                   _requestGenerator (
                                                                           Factory<RequestGenerator>::create (
                                                                                   config[ "requestGeneratorConfig" ] )),
                                                                   _thinkTimeDistribution (
                                                                           Factory<Distribution>::create (
                                                                                   config[ "thinkTime" ] )) {

    _MPL = config[ "MPL" ].asUInt ( );


}

RequestStreamClosed::~RequestStreamClosed ( ) {


}


void RequestStreamClosed::init ( ) {

    for ( unsigned int i = 0; i < _MPL; i++ ) {
        addArrivalEvent ( );
    }

}

Request *RequestStreamClosed::next ( ) {

    uint64_t currentTime = simulator::getSimTime ( );

    uint64_t thinkTime = convertSecondsToTime ( _thinkTimeDistribution->nextRand ( ));

    Request *nextRequest = _requestGenerator->nextRequest ( currentTime + thinkTime );

    return nextRequest;


}

void RequestStreamClosed::notifyEnd ( Request *req ) {

    RequestStream::notifyEnd ( req );
    addArrivalEvent ( );


}