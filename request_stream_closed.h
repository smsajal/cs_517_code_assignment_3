#ifndef REQUEST_STREAM_CLOSED_H
#define REQUEST_STREAM_CLOSED_H

#include "json/json.h"
#include "request_stream.h"
#include "distribution.h"
#include "request_generator.h"

// Closed-loop request sequence
class RequestStreamClosed : public RequestStream {
private:
    //TODO fill in
    //have a private distribution object here that will be created with the factory...
    RequestGenerator *_requestGenerator;
    Distribution *_thinkTimeDistribution;
    unsigned int _MPL;


public:
    RequestStreamClosed ( Json::Value &config );

    virtual ~RequestStreamClosed ( );

    //TODO fill in
    //in the init method, create MPL number of jobs (refer to the config file), so that there are always MPL number of jobs in the system
    virtual void init();

    // follow the req_stream_open, but with the think-time and arrival time---- change accordingly
    virtual Request *next();

    virtual void notifyEnd ( Request *req );
};

#endif /* REQUEST_STREAM_CLOSED_H */
