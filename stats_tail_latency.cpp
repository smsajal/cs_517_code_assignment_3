#include <iostream>
#include <string>
#include <algorithm>
#include "stats_tail_latency.h"
#include "time_helper.h"
#include "factory.h"

REGISTER_CLASS( Stats, StatsTailLatency )

StatsTailLatency::StatsTailLatency( Json::Value &config )
        : Stats( config ) {
    Json::Value &percentiles = config[ "percentiles" ];
    for ( unsigned int i = 0 ; i < percentiles.size( ) ; i++ ) {
        _percentiles.push_back( percentiles[ i ].asDouble( ));
    }
    resetStats( );
}

StatsTailLatency::~StatsTailLatency( ) {
}

// Reset stats for current interval
void StatsTailLatency::resetStats( ) {
    //TODO fill in

    _latencyTimes.clear();

}

// Called when a request is completed by a target
void StatsTailLatency::notifyEnd( Request *req ) {
//    std::cout<<"notify end starts....\n";
    if ( checkOutputStats( )) {
        resetStats( );
    }

    //TODO fill in
    uint64_t elapsedTime = req->getElapsedTime( );
    _latencyTimes.push_back( elapsedTime );
//    std::cout<<"notify end ends....\n";

}

double StatsTailLatency::getTailLatency( double p ) {
    //TODO fill in
//    std::cout<<"get tail latency start....\n";
    uint64_t numberOfEntries = _latencyTimes.size( );
    if(numberOfEntries==0){
        return 0.0;
    }
    uint64_t tailPercentileIndex = getTailPercentileIndex( p, numberOfEntries );

    std::nth_element( _latencyTimes.begin( ), (_latencyTimes.begin( ) + (int)tailPercentileIndex), _latencyTimes.end( ));

    double tailLatency = convertTimeToSeconds( _latencyTimes[ tailPercentileIndex ] );
//    std::cout<<"get tail latency end....\n";
    return tailLatency;

}

void StatsTailLatency::printStats( uint64_t lastIntervalBegin, uint64_t lastIntervalEnd ) {
    std::cout << "t " << lastIntervalBegin;
    for ( double p : _percentiles ) {
        std::cout << " " << p << " " << getTailLatency( p );
    }
    std::cout << std::endl;
}

Json::Value StatsTailLatency::jsonStats( uint64_t lastIntervalBegin, uint64_t lastIntervalEnd ) {
    Json::Value results;
    results[ "t" ] = lastIntervalBegin;
    for ( double p : _percentiles ) {
        results[ std::to_string( p ) ] = getTailLatency( p );
    }
    return results;
}
